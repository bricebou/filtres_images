# Changelog

## Unreleased

### Added

- Installable en tant que package Composer
- spip/spip#5925 Filtre `image_oriente_selon_exif()` pour réorienter automatiquement une image selon son exif

### Changed

- spip/spip#5925 Les filtres d’images tel que `image_recadre` réorientent l’image selon l’exif d’orientation
- Compatible SPIP 5.0.0-dev

### Fixed

- !4724 Optimisation du filtre `image_aplatir()`
- !4723 Optimisation du filtre `image_renforcement()`
- !4722 Optimisation du filtre `image_flou()`
- !4721 Optimisation du filtre `image_sepia()`
- !4718 Optimisation des filtres `image_flip_vertical()` & `image_flip_horizontal()`
- !4720 Optimisation du filtre `image_nb()`
- !4719 Optimisation du filtre `image_gamma()`
- #4716 Optimisation du filtre `image_rotation()`
- #4716 Correction du paramètre `crop` de `image_rotation()`
