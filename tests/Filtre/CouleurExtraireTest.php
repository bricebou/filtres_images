<?php

declare(strict_types=1);

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

namespace Spip\Test\Images\Funct;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CouleurExtraireTest extends TestCase
{
	public static function setUpBeforeClass(): void {
		find_in_path('filtres/couleurs.php', '', true);
	}

	#[DataProvider('providerCouleurExtraire')]
	public function testCouleurExtraire($expected, ...$args): void {
		$actual = couleur_extraire(...$args);
		$this->assertSame($expected, $actual);
	}

	public static function providerCouleurExtraire(): array {
		return [
			'valeur_par_défaut' =>
			[
				0 => 'F26C4E',
				1 => '',
			],
			'extraction_image_chemin_relatif' =>
			[
				0 => '739cc8',
				1 => find_in_path('tests/data/degrade-bleu.jpg'),
			],
			# Echoue si https local mal configuré
			/*'extraction_image_absolu' =>
			[
				0 => '739cc8',
				1 => url_absolue(find_in_path('tests/data/degrade-bleu.jpg'), $GLOBALS['meta']['adresse_site'] . '/'),
			],*/
		];
	}
}
