<?php

declare(strict_types=1);

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

namespace Spip\Test\Images\Funct;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MultipleDeTroisTest extends TestCase
{
	public static function setUpBeforeClass(): void {
		find_in_path('filtres/images_lib.php', '', true);
	}

	#[DataProvider('providerMultipleDeTrois')]
	public function testMultipleDeTrois($expected, ...$args): void {
		$actual = multiple_de_trois(...$args);
		$this->assertSame($expected, $actual);
	}

	public static function providerMultipleDeTrois(): array {
		return [
			0 => [
				0 => 0,
				1 => 0,
			],
			1 => [
				0 => -0,
				1 => -1,
			],
			2 => [
				0 => 0,
				1 => 1,
			],
			3 => [
				0 => 3,
				1 => 2,
			],
			4 => [
				0 => 3,
				1 => 3,
			],
			5 => [
				0 => 3,
				1 => 4,
			],
			6 => [
				0 => 6,
				1 => 5,
			],
			7 => [
				0 => 6,
				1 => 6,
			],
			8 => [
				0 => 6,
				1 => 7,
			],
			9 => [
				0 => 9,
				1 => 10,
			],
			10 => [
				0 => 21,
				1 => 20,
			],
			11 => [
				0 => 30,
				1 => 30,
			],
			12 => [
				0 => 51,
				1 => 50,
			],
			13 => [
				0 => 99,
				1 => 100,
			],
			14 => [
				0 => 999,
				1 => 1000,
			],
			15 => [
				0 => 9999,
				1 => 10000,
			],
		];
	}
}
