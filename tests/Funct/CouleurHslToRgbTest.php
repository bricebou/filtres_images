<?php

declare(strict_types=1);

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

namespace Spip\Test\Images\Funct;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CouleurHslToRgbTest extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		find_in_path('filtres/images_lib.php', '', true);
	}

	#[DataProvider('providerCouleurHslToRgb')]
	public function testCouleurHslToRgb($expected, ...$args): void
	{
		$actual = _couleur_hsl_to_rgb(...$args);
		$this->assertEqualsWithDelta($expected, $actual, 0.000001);
	}

	#[DataProvider('providerCouleurHslToRgb')]
	public function testCouleurHslToRgbDeprecated($expected, ...$args): void
	{
		$actual = _couleur_hsl2rgb(...$args);
		$this->assertEqualsWithDelta($expected, $actual, 0.000001);
	}

	public static function providerCouleurHslToRgb(): array
	{
		return [
			0 =>
			[
				0 =>
				[
					'r' => 0,
					'g' => 0,
					'b' => 0,
				],
				1 => 0,
				2 => 0,
				3 => 0,
			],
			1 =>
			[
				0 =>
				[
					'r' => 63,
					'g' => 63,
					'b' => 63,
				],
				1 => 0,
				2 => 0,
				3 => 0.25,
			],
			2 =>
			[
				0 =>
				[
					'r' => 127,
					'g' => 127,
					'b' => 127,
				],
				1 => 0,
				2 => 0,
				3 => 0.5,
			],
			3 =>
			[
				0 =>
				[
					'r' => 191,
					'g' => 191,
					'b' => 191,
				],
				1 => 0,
				2 => 0,
				3 => 0.75,
			],
			4 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 255,
					'b' => 255,
				],
				1 => 0,
				2 => 0,
				3 => 1,
			],
			5 =>
			[
				0 =>
				[
					'r' => 0,
					'g' => 0,
					'b' => 0,
				],
				1 => 0,
				2 => 0.25,
				3 => 0,
			],
			6 =>
			[
				0 =>
				[
					'r' => 79,
					'g' => 47,
					'b' => 47,
				],
				1 => 0,
				2 => 0.25,
				3 => 0.25,
			],
			7 =>
			[
				0 =>
				[
					'r' => 159,
					'g' => 95,
					'b' => 95,
				],
				1 => 0,
				2 => 0.25,
				3 => 0.5,
			],
			8 =>
			[
				0 =>
				[
					'r' => 207,
					'g' => 175,
					'b' => 175,
				],
				1 => 0,
				2 => 0.25,
				3 => 0.75,
			],
			9 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 255,
					'b' => 255,
				],
				1 => 0,
				2 => 0.25,
				3 => 1,
			],
			10 =>
			[
				0 =>
				[
					'r' => 0,
					'g' => 0,
					'b' => 0,
				],
				1 => 0,
				2 => 0.5,
				3 => 0,
			],
			11 =>
			[
				0 =>
				[
					'r' => 95,
					'g' => 31,
					'b' => 31,
				],
				1 => 0,
				2 => 0.5,
				3 => 0.25,
			],
			12 =>
			[
				0 =>
				[
					'r' => 191,
					'g' => 63,
					'b' => 63,
				],
				1 => 0,
				2 => 0.5,
				3 => 0.5,
			],
			13 =>
			[
				0 =>
				[
					'r' => 223,
					'g' => 159,
					'b' => 159,
				],
				1 => 0,
				2 => 0.5,
				3 => 0.75,
			],
			14 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 255,
					'b' => 255,
				],
				1 => 0,
				2 => 0.5,
				3 => 1,
			],
			15 =>
			[
				0 =>
				[
					'r' => 0,
					'g' => 0,
					'b' => 0,
				],
				1 => 0,
				2 => 0.75,
				3 => 0,
			],
			16 =>
			[
				0 =>
				[
					'r' => 0,
					'g' => 0,
					'b' => 0,
				],
				1 => 1,
				2 => 1,
				3 => 0,
			],
			17 =>
			[
				0 =>
				[
					'r' => 127,
					'g' => 0,
					'b' => 0,
				],
				1 => 1,
				2 => 1,
				3 => 0.25,
			],
			18 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 0,
					'b' => 0,
				],
				1 => 1,
				2 => 1,
				3 => 0.5,
			],
			19 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 127,
					'b' => 127,
				],
				1 => 1,
				2 => 1,
				3 => 0.75,
			],
			20 =>
			[
				0 =>
				[
					'r' => 255,
					'g' => 255,
					'b' => 255,
				],
				1 => 1,
				2 => 1,
				3 => 1,
			],
		];
	}
}
