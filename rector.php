<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Php81\Rector\ClassConst\FinalizePublicClassConstantRector;
use Rector\Php81\Rector\FuncCall\NullToStrictStringFuncCallArgRector;
use Rector\Php71\Rector\FuncCall\CountOnNullRector;
use Rector\Php80\Rector\FunctionLike\MixedTypeRector;
use Rector\Php80\Rector\FunctionLike\UnionTypesRector;

return static function (RectorConfig $rectorConfig): void {
	$rectorConfig->paths([
		__DIR__ . '/filtres',
		__DIR__ . '/images_fonctions.php',
		__DIR__ . '/tests',
	]);

	$rectorConfig->sets([
		LevelSetList::UP_TO_PHP_81
	]);

	$rectorConfig->skip([
		__DIR__ . '/lang',
		FinalizePublicClassConstantRector::class,
		NullToStrictStringFuncCallArgRector::class,
		CountOnNullRector::class,
		MixedTypeRector::class,
		UnionTypesRector::class,
	]);
};
